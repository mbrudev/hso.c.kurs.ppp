/**
@file: Kap03_Auf02.c
@brief: File contains main program
@author: Mike
@date: 27.10.2017
*/

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

/**
@fn int main(void)
@brief Print '=' character in four diffrent ways
@return EXIT_SUCCESS (0)
@author Mike
@date 27.10.2017
*/

int main(void)
{
   char *ch = "=";
   printf("%s\n", ch);
   printf("0x3D\n");
   printf("61\n");
   printf("'='");
   _getch();

   return EXIT_SUCCESS;
}


