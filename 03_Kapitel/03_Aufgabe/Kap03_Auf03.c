/**
@file: Kap03_Auf03.c
@brief: File contains main program
@author: Mike
@date: 27.10.2017
*/

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

/**
@fn int main(void)
@brief This function calculates an equation
@param void
@return EXIT_SUCCESS (0)
@author Mike
@date 27.10.2017
*/

int main(void)
{
   float a = 0.0F;
   float b = 0.0F;

   printf("Geben Sie zwei  Zahlen ein\n");

   scanf_s("%f", &a);
   scanf_s("%f", &b);
   float c = a - 4.5 * b - b * 10.2;

   printf("%f = c", c);
   _getch();

   return EXIT_SUCCESS;
}