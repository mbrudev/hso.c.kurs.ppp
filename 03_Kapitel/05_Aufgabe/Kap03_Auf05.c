/**
@file: Kap03_Auf05.c
@brief: File contains main program
@author: Mike
@date: 27.10.2017
*/

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

/**
@fn int main(void)
@brief Print hexadecimal number as decimal number
@param void
@return EXIT_SUCCESS (0)
@author Mike
@date 27.10.2017
*/

int main(void)
{
   unsigned int usiTest1 = 0x7FFFFFFF;
   signed int siTest1 = usiTest1;
   printf("%d\n", siTest1);

   unsigned int usiTest2 = 0xFFFFFFFF;
   signed int siTest2 = usiTest2;
   printf("%d\n", siTest2);
   _getch();

   return EXIT_SUCCESS;
}