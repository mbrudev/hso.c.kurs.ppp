/**
@file: Kap03_Auf04.c
@brief: File contains main program
@author: Mike
@date: 27.10.2017
*/

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <conio.h>

/**
@fn int main(void)
@brief Print Min and Max Integer values
@param void
@return EXIT_SUCCESS (0)
@author Mike
@date 27.10.2017
*/

int main(void)
{
   //Define int min and max Value;
   int iWertMin = INT_MIN;
   int iWertMax = INT_MAX;

   printf("Min Wert -0: = %i, \nMin Wert -1: = %i, \nMax Wert +0: = %i, \nMax Wert +1: = %i",
          iWertMin,
          (iWertMin - 1),
          iWertMax,
          (iWertMax + 1));
   _getch();
   return EXIT_SUCCESS;
}