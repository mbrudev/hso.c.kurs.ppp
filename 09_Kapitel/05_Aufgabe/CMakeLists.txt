cmake_minimum_required(VERSION 3.8)
project(05_Aufgabe)

set(CMAKE_C_STANDARD 99)

include_directories(headers ${SOURCE_FILES})
include_directories(lib ${SOURCE_FILES})

set(SOURCE_FILES main.c print_Board.c headers/linux-utilities.h)
add_executable(05_Aufgabe ${SOURCE_FILES})



#target_link_libraries(05_Aufgabe I:\\repo\\hso.c.kurs.ppp\\09_Kapitel\\05_Aufgabe\\lib\\debug-utils.lib)



