/**
@file: main.c
@brief: Contains main program
@author: Mike
@date: 02.12.2017
*/

#include <stdio.h>
#include <stdlib.h>

#define ROW 10
#define COL 10

void printBoard(int ai[ROW][COL]);

int main(void)
{
   int aiDim[10][10];
   printBoard(aiDim);
}