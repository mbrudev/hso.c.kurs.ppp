/**
@file print_Board.c
@brief Print Board
@author Mike 
@date 02.12.2017
*/

#include <stdio.h>
//#include "headers/linux-utilities.h"
#include "headers/utilities.h"

#define ROW 10
#define COL 10

void printBoard(int ai[ROW][COL])
{
   int iValues = 0;

   for (int i = 0; i < ROW; i++) {
      _gotoxy(80, (short) (25 + i));
      for (int j = 0; j < COL; j++) {
         ai[i][j] = iValues;
         iValues++;
         printf("%02d ", ai[i][j]);
      }
      printf("\n");
   }
}
