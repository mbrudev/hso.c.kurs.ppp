/*!
 * @file utilities.c
 * @brief Linux implemtation of Badura/Fischer's utility functions.
 * @author Robin Willmann
 * @date 09.10.2017
 ************************************************************************/
 // This file was meant for free use.
#include <stdio.h>
#include <stdlib.h>


#define die(errormsg) \
        {\
          printf("%s:%s:%d : %s\n", __FILE__, __func__, __LINE__, errormsg);\
          exit(1);\
        }

//with origin at (0,0)
void _gotoxy(short x , short y)
{
  if(x < 0 || y < 0)
    die("Illegal Argument. x and y must be both >= 0 since origin is at (0,0).");

  // Windows uses (0,0) as origin where as the origin with Linux
  // console_codes is (1,1). therefore one has to be added.

  // Docs for windows.h Coords
  // https://docs.microsoft.com/en-us/windows/console/coord-str

  // for details about these 'magic' strings see `man console_codes`.
  printf("\x1B[%d;%dH", x+1, y+1);
}

void _clrscr(void)
{
  printf("\x1B[H\x1B[J");
}