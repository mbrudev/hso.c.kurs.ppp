/**
@file: Kap09_Auf03.c
@brief: Contains main program
@author: Mike
@date: 02.12.2017
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct person
{
   char cFirstName[50];
   char cLastName[50];
   unsigned int cBirthday;
} structperson = {"Max", "Mustermann", 1990};


void f1Value(char[], char[], unsigned int uiBday);
void f2Value(struct person s);

void f1Reference(char *, char *, unsigned int *);
void f2Reference(struct person *);

int main(void)
{
   f1Value(structperson.cFirstName, structperson.cLastName, structperson.cBirthday);
   f2Value(structperson);

   f1Reference(structperson.cFirstName, structperson.cLastName, &structperson.cBirthday);
   f2Reference(&structperson);
}

/**
@fn void f1Value(char cFn[], char cLn[], unsigned int uiBday)
@brief Print struct as Call by Value
@param cFn cLn uiBday
@return
@author Mike
@date 02.12.2017
*/
void f1Value(char cFn[], char cLn[], unsigned int uiBday)
{
   uiBday = 1995;
   cLn = "Bruder";
   cFn = "Mike";
   printf("\nf1Value()\nFirstName: %s\nLastName: %s\nBirthday: %u\n",
          structperson.cFirstName,
          structperson.cLastName,
          structperson.cBirthday);
}

/**
@fn void f2Value(struct person s)
@brief Print struct as Call by Value
@param s
@return
@author Mike
@date 02.12.2017
*/
void f2Value(struct person s)
{
   s.cBirthday = 1995;
   strcpy(s.cLastName, "Bruder");
   strcpy(s.cFirstName, "Mike");
   printf("\nf2Value()\nFirstName: %s\nLastName: %s\nBirthday: %u\n",
          structperson.cFirstName,
          structperson.cLastName,
          structperson.cBirthday);
}

/**
@fn void f1Reference(char *cFn, char *cLn, unsigned int *uiBday)
@brief Print struct as Call by Reference
@param cFn cLn uiBday
@return
@author Mike
@date 02.12.2017
*/
void f1Reference(char *cFn, char *cLn, unsigned int *uiBday)
{
   *uiBday = 1995;
   strcpy(cLn, "Bruder");
   strcpy(cFn, "Mike");
   printf("\nf1Reference()\nFirstName: %s\nLastName: %s\nBirthday: %u\n",
          structperson.cFirstName,
          structperson.cLastName,
          structperson.cBirthday);
}

/**
@fn void f2Reference(struct person *ps)
@brief Print struct as Call by Reference
@param ps
@return
@author Mike
@date 02.12.2017
*/
void f2Reference(struct person *ps)
{
   ps->cBirthday = 1995;
   strcpy(ps->cLastName, "Bruder");
   strcpy(ps->cFirstName, "Mike");
   printf("\nf2Reference()\nFirstName: %s\nLastName: %s\nBirthday: %u\n",
          structperson.cFirstName,
          structperson.cLastName,
          structperson.cBirthday);
}