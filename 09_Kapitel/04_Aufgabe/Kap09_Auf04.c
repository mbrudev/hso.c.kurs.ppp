/**
@file: Kap09_Auf04.c
@brief: Contains main program
@author: Mike
@date: 02.12.2017
*/

#include <stdio.h>
#include <stdlib.h>

struct values
{
   char *pcString; // 4 Byte in MSVC
   char acArray[4]; // 4 Byte
   char cSign; // 1 Byte
   int iInteger; // 4 Byte
};

int main(void)
{
   struct values structvalues;
   printf("\n*pcString: %p", &structvalues.pcString);
   printf("\nacArray[4]: %p", &structvalues.acArray);
   printf("\ncSign: %p", (void *) &structvalues.cSign);
   printf("\niInteger: %p", &structvalues.iInteger);
   printf("\n\nSIZEOF: %i", (int) sizeof(struct values));

}