/**
@file: Kap09_Auf06.c
@brief: Contains main program
@author: mike
@date: 12/6/17
*/
#include <stdio.h>
#include <stdlib.h>

/**
@fn int main(void)
@brief print array
@param void
@return EXIT_SUCCESS (0)
@author mike
@date 12/6/17
*/
int main(void)
{
   unsigned int *uiSize;
   uiSize = malloc((100 * sizeof(unsigned int)));

   for (int i = 0; i < 100; i++) {
      uiSize[i] = (unsigned int) (i);
      uiSize[42] = 888;
      printf("%i\n", uiSize[i]);
   }
   free(uiSize);

   return EXIT_SUCCESS;
}