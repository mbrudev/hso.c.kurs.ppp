/**
@file: Kap09_Auf02.c
@brief:Contains main program
@author: mike
@date: 11/30/17
*/

#include <stdio.h>
#include <stdlib.h>

int fillArray(int []);
int fillArray2(int* pi);
int fillArray3(int ai[5]);

/**
@fn int main(void)
@brief main function
@param void
@return EXIT_SUCCESS (0)
@author mike
@date 11/30/17
*/

int main(void)
{
   int ai[5];
   fillArray(ai);
   fillArray2(ai);
   fillArray3(ai);
   return EXIT_SUCCESS;
}

/**
@fn int fillArray(int aiFa[5])
@brief Fill Array
@param aiFa
@return
@author mike
@date 11/30/17
*/
int fillArray(int aiFa[])
{
   for (int i = 0; i < 5; i++) {
      aiFa[i] = i + 1;
      printf("1. Init Array: %i\n", aiFa[i]);
   }

   int *pi = &aiFa[2];
   printf("1. Print Pointer: %i\n", *pi);
   *pi = *pi + 3;
   printf("1. Print Pointer + 3: %i\n", *pi);
}

/**
@fn int fillArray2(int* pi)
@brief Fill Array with Pointer Parameter
@param pi
@return
@author mike
@date 12/4/17
*/
int fillArray2(int* pi)
{
   for (int i = 0; i < 5; i++) {
      pi[i] = i + 1;
      printf("2. Init Array: %i\n", pi[i]);
   }

   pi = &pi[2];
   printf("2. Print Pointer: %i\n", *pi);
   *pi = *pi + 3;
   printf("2. Print Pointer + 3: %i\n", *pi);
}

/**
@fn int fillArray3(int ai[5])
@brief Fill Array with fixed Array size
@param ai
@return
@author mike
@date 12/4/17
*/
int fillArray3(int ai[5])
{
   for (int i = 0; i < 5; i++) {
      ai[i] = i + 1;
      printf("3. Init Array: %i\n", ai[i]);
   }

   int *pi = &ai[2];
   printf("3. Print Pointer: %i\n", *pi);
   *pi = *pi + 3;
   printf("3. Print Pointer + 3: %i\n ", *pi);
}