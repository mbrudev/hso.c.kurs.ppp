/**
@file Kap09_Auf09b.c
@brief
@author Mike 
@date 02.12.2017
*/

#include <stdio.h>
#include <stdlib.h>
#include "headers/utilities.h"

void printSeperator();
int menuLayout();

int print_menu()
{
   char acTitle[] = "Pointer Functions";
   printSeperator();
   printf("Press a, b or c to call %s. Press e to Exit %s\n" , acTitle, acTitle);
   printSeperator();
   menuLayout();
   return EXIT_SUCCESS;
}

void printSeperator(void)
{
   for (int i = 0; i < 50; i++) {
      printf("-");
   }
   printf("\n");
}

int menuLayout()
{
   char *cpArray[4] = {"-a- (calls function a)", "-b- (calls function b)", "-c- (calls function c)", "-e- (Exit)"};

   for (int i = 0; i < ((sizeof(cpArray)) / (sizeof(char *))); i++) {

      if ((i + 2) % 2 == 0) {
         _gotoxy(0, ((short) (i + 2))); // x = 0 y = 2 Beginn 0 + 2 = 2
         printf("\n%s", cpArray[i]);  // x = 0 y = 4  Beginn 2 + 2 = 4
      }
      else {
         _gotoxy(2*(sizeof(cpArray)), ((short) (i + 2)));
         printf("%s\n", cpArray[i]);
      }

   }
   printSeperator();
   return EXIT_SUCCESS;
}
