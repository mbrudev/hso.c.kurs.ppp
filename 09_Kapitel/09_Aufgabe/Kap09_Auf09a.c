/**
@file: Kap09_Auf09a.c
@brief: Contains main program
@author: mike
@date: 12/6/17
*/
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <conio.h>
#include "headers/utilities.h"


int print_menu();
void statusLayout(void (*pf)(void));

void fa(void);
int fb(void);
int *fc(int i);

/**
@fn int main(void)
@brief main function
@param void
@return
@author mike
@date 12/6/17
*/
int main(void)
{
   char const ccExit = 'e';
   char retVal = -1;
   char input;

   while (retVal != ccExit) {
      print_menu();
      input = (char) _getch();

      switch (tolower(input)) {
         case 'e': input = ccExit;
            break;
         case 'a': statusLayout(&fa);
            system("\npause\n");
            break;
         case 'b': statusLayout(&fb);
            system("\npause\n");
            break;
         case 'c': statusLayout(&fc);
            system("\npause\n");
            break;

         default: printf("No Valid Key");
            break;
      }
      retVal = input;
      _clrscr();
   }
   return retVal;
}
