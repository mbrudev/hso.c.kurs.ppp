/**
@file Kap09_Auf09c.c
@brief
@author Mike 
@date 02.12.2017
*/

#include <stdio.h>
#include <stdlib.h>


void printSeperator();

void statusLayout(void (*pf)(void))
{
   printSeperator();
   pf();
}

void fa(void)
{
   printf("fa\n");
}

int fb(void)
{
   printf("fb\n");
   return EXIT_SUCCESS;
}

int* fc(int i)
{
   printf("fc %i\n", i);
   return EXIT_SUCCESS;
}