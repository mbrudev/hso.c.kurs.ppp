/**
@file: Kap09_Auf01.c
@brief: Contains main program
@author: mike
@date: 11/30/17
*/

#include <stdio.h>
#include <stdlib.h>

int main(void)
{
   int iA;
   int iB;
   int iC;

   printf("%p\n", &iA);
   printf("%p\n", &iB);
   printf("%p\n", &iC);
}