/**
@file: Kap09_Auf08.c
@brief: Contains main program
@author: mike
@date: 12/6/17
*/

#include <stdio.h>
#include <stdlib.h>

int printArr(int *pi, int size);
int buublesort(int *bs, int size);

/**
@fn int main(void)
@brief main function
@param void
@return
@author mike
@date 12/6/17
*/

int main(void)
{
   int *piSize;
   int *piWrite;
   piSize = malloc((100 * sizeof(int)));
   piWrite = piSize;

   for (int i = 0; i < 100; i++) {
//      *(piWrite + i) = 100 - i; alternativ
      piWrite[i] = 100 - i;
   }
   printArr(piSize, 100);
   buublesort(piSize, 100);
}

/**
@fn int printArr(int *pi, int size)
@brief Print Array
@param pi size
@return
@author mike
@date 12/6/17
*/
int printArr(int *pi, int size)
{
   for (int i = 0; i < size; i++) {
      *pi = pi[i];
      printf("%i\n", *pi);
   }
}

/**
@fn int buublesort(int *bs, int size)
@brief
@param
@return
@author mike
@date 12/6/17
*/
int buublesort(int *bs, int size)
{
   unsigned int uiHelper;

   for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
         if (bs[i] < bs[j]) {
            uiHelper = (unsigned) bs[i];
            bs[i] = bs[j];
            bs[j] = uiHelper;
         }
      }
   }
   printf("\nBubblesort:\n");
   for (int k = 0; k < size; k++) {
      bs[k] = k + 1;
      printf("%i\n", bs[k]);
   }
}