/**
@file: Kap05_Auf02.c
@brief: Contains main program
@author: Mike
@date: 04.11.2017
*/
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

/**
@fn main(void)
@brief Call functions lastName() firstName() address()
@param void
@return EXIT_SUCCESS (0)
@author Mike
@date 04.11.2017
*/

int firstName(void);
int lastName(void);
int address(void);

int main(void)
{
   firstName();
   lastName();
   address();
   _getch();
   return EXIT_SUCCESS;
}

/**
@fn firstName(void)
@brief Print first name
@param void
@return EXIT_SUCCESS (0)
@author Mike
@date 04.11.2017
*/
int firstName(void)
{
   printf("First Name: Mike\n");
   return EXIT_SUCCESS;
}

/**
@fn lastName(void)
@brief Print last name
@param void
@return EXIT_SUCCESS (0)
@author Mike
@date 04.11.2017
*/
int lastName(void)
{
   printf("Last Name: Bruder\n");
   return EXIT_SUCCESS;
}

/**
@fn address(void)
@brief Print address
@param void
@return EXIT_SUCCESS (0)
@author Mike
@date 04.11.2017
*/
int address(void){
   printf("Address: Offenburg");
   return EXIT_SUCCESS;
}