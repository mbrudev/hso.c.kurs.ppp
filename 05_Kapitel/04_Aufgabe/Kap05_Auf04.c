/**
@file: Kap05_Auf04.c
@brief: Contains main program
@author: Mike
@date: 04.11.2017
*/

#define _USE_MATH_DEFINES
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <math.h>

//Function Declaration
double getVolumeOfSphere(double dr);
double testCase(void);

/**
@fn int main(void)
@brief
@param void
@return EXIT_SUCCESS (0)
@author Mike
@date 04.11.2017
*/
int main(void)
{
   const double dr = testCase();
   getVolumeOfSphere(dr); //Call Function
   _getch();
   return EXIT_SUCCESS;
}

/**
@fn double getVolumeOfSphere(double dr)
@brief Definition double getVolumeOfSphere(double dr)
@param double dr
@return double dVolume
@author Mike
@date 04.11.2017
*/
double getVolumeOfSphere(double dr)
{
   double dVolume = (4 * M_PI * powl(dr, 3)) / 3;
   return dVolume;
}

/**
@fn double testCase(void)
@brief Definition testCase(void)
@param void
@return EXIT_SUCCESS (0)
@author Mike
@date 04.11.2017
*/
double testCase(void)
{
   printf("Start testCase method:\n");
   for (int i = 1; i <= 3; ++i)
   {
      printf("testCase Result for positve integer Radius = %i: = %lf\n", i, getVolumeOfSphere(i));
      printf("testCase Result for negative integer Radius = %i: = %lf\n", i, getVolumeOfSphere((-1 * i)));
      printf("testCase Result for random number Radius = %i: = %lf\n", i, getVolumeOfSphere(i * rand()));
   }
   return EXIT_SUCCESS;
}
