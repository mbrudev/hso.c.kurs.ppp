/**
@file: Kap05_Auf01.c
@brief: Contains main program
@author: Mike
@date: 04.11.2017
*/
#include <stdio.h>
#include <stdlib.h>

/**
@fn main(void)
@brief Print "Last Name, First Name, Address"
@param void
@return EXIT_SUCCESS (0)
@author Mike
@date 04.11.2017
*/

int main()
{

   printf("Mike\n");
   printf("Bruder\n");
   printf("Offenburg");

   return EXIT_SUCCESS;
}