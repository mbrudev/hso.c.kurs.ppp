/**
@file Kap05_Auf03.c
@brief Contains main program
@author Mike
@date 04.11.2017
*/

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

int f11(void);
int f12(int iA);
int f13(void);
int f21(void);
int f22(int iB);
int f32(int iC);

/**
@fn main(void)
@brief Call method f11() f12() f13()
@param void
@return EXIT_SUCCESS (0)
@author Mike
@date 04.11.2017
*/
int main(void)
{
   f11();
   f12(12);
   f13();
   _getch();
   return EXIT_SUCCESS;
}

/**
@fn int f11(void)
@brief Call method f21()
@param void
@return EXIT_SUCCESS (0)
@author Mike
@date 04.11.2017
*/
int f11(void)
{
   f21();
   return EXIT_SUCCESS;
}

/**
@fn int f12(int iA)
@brief Save parameter int iA in int iF, call method f22(), return iF
@param int iA
@return int iF
@author Mike
@date 04.11.2017
*/
int f12(int iA)
{
   int iF = iA;
   printf("Expected Value iF: 12 = %i\n", iF);
   f22(22);
   return iF;
}

/**
@fn int f13(void)
@brief void function
@param void
@return EXIT_SUCCESS (0)
@author Mike
@date 04.11.2017
*/
int f13(void)
{
   return EXIT_SUCCESS;
}

/**
@fn int f21(void)
@brief void function
@param void
@return EXIT_SUCCESS (0)
@author Mike
@date 04.11.2017
*/
int f21(void)
{
   return EXIT_SUCCESS;
}

/**
@fn int f22(int iB)
@brief Save parameter int iB in int iE, call method f32(), return int iE
@param int iB
@return int iE
@author Mike
@date 04.11.2017
*/
int f22(int iB)
{
   int iE = iB;
   printf("Expected Value iE: 22 = %i\n", iE);
   f32(32);
   return iE;
}

/**
@fn int f32(int iC)
@brief Save parameter int iC in int iD and return int iD
@param int iC
@return int iD
@author Mike
@date 04.11.2017
*/
int f32(int iC)
{
   int iD = iC;
   printf("Expected Value iD: 32 = %i\n", iD);
   return iD;
}