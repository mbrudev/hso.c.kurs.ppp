/**
@file: Kap10_Auf03.c
@brief: Contains main program
@author: Mike
@date: 08.12.2017
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

static char *pscCR = "0x0D";

struct getFile
{
   size_t iLength;
   char *cFilename;
   char *cExtension;
} structGetFile[2];

struct getFile fInput();
int fInitFile(struct getFile *, struct getFile *);

/**
@fn int main(void)
@brief main function
@param void
@return EXIT_SUCCESS (0)
@author Mike
@date 10.12.2017
*/
int main(void)
{
   printf("Convert File Dos2Unix!\n\n");
   fInput();
   printf("0: %s 0: %s 0: %i\n", structGetFile[0].cFilename, structGetFile[0].cExtension, structGetFile[0].iLength);
   printf("1: %s 1: %s 1: %i\n", structGetFile[1].cFilename, structGetFile[1].cExtension, structGetFile[1].iLength);
   fInitFile(&structGetFile[0], &structGetFile[1]);
   return EXIT_SUCCESS;
}

/**
@fn struct getFile fInput()
@brief Get user input and write to struct structGetFile
@param void
@return structGetFile[siFileSize]
@author Mike
@date 10.12.2017
*/
struct getFile fInput(void)
{
   char *pcPlatform[] = {"DOS", "UNIX"};
   for (int i = 0; i < 2; i++) {
      printf("Enter your '%s' File\n", pcPlatform[i]);
      structGetFile[i].cFilename = (char *) malloc((sizeof(structGetFile)));
      printf("sizeof structGetFile %d\n", sizeof(structGetFile));
      structGetFile[i].cExtension = (char *) malloc((sizeof(structGetFile)));

      scanf("%s", structGetFile[i].cFilename);
      strcpy(structGetFile[i].cExtension, ".txt");
      structGetFile[i].iLength = (size_t) 1 + ((strlen(structGetFile[i].cFilename) + strlen(structGetFile[i].cExtension)));
   }
   return structGetFile[2];
}

/**
@fn int fInitFile(struct getFile *fileInput, struct getFile *fileOutput)
@brief Read DOS Input file and convert to UNIX Output File
@param struct getFile *fileInput, struct getFile *fileOutput
@return EXIT_SUCCESS (0)
@author Mike
@date 10.12.2017
*/
int fInitFile(struct getFile *fileInput, struct getFile *fileOutput)
{
   FILE *pfInput, *pfOutput;
   char cBuff[FILENAME_MAX];
   char *pcInput = (char *) malloc(1 + (fileInput->iLength));
   char *pcOutput = (char *) malloc(1 + (fileOutput->iLength));


   strcpy(pcInput, strcat(fileInput->cFilename, fileInput->cExtension));
   strcpy(pcOutput, strcat(fileOutput->cFilename, fileOutput->cExtension));
   printf("Input File: %s\nOutput File: %s\n", pcInput, pcOutput);

   pfInput = fopen(pcInput, "rb");
   pfOutput = fopen(pcOutput, "wb+");

   if ((pfInput != NULL)) {
      while (fgets(cBuff, 255, pfInput) != NULL) {
         if ((strcmp(pscCR, cBuff) == 0)) {
            fprintf(pfOutput, 0);
         }
         else {
            fprintf(pfOutput, cBuff);
         }
      }
      fclose(pfInput);
      fclose(pfOutput);
      printf("\nProcess finished\n");
   }
   return EXIT_SUCCESS;
}


