/**
@file: Kap10_Auf01.c
@brief: Contains main program
@author: Mike
@date: 08.12.2017
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
@fn void main(void)
@brief main function
@param void
@return
@author Mike
@date 08.12.2017
*/
void main(void)
{
   FILE *pf;
   char acLine[FILENAME_MAX];
   char *pcExtension = ".csv";
   char cFilename[FILENAME_MAX + strlen(pcExtension)];
   char cDelimiter[] = ",;";
   char *pToken;

   printf("Choose File to open (Without file Extension!\n");
   scanf("%s", cFilename);
   strcat(cFilename, pcExtension);
   pf = fopen(cFilename, "r");

   if (pf != NULL) {
      while (feof(pf) == NULL) {
         fgets(acLine, 255, pf); // file gets
         printf("Print acLine: %s\n\n", acLine);
         pToken = strtok(acLine, cDelimiter);

         while (pToken != NULL) {
            printf("Print pToken: %s\n", pToken);
            pToken = strtok(NULL, cDelimiter);
         }
      }
      fclose(pf);
   }
}
