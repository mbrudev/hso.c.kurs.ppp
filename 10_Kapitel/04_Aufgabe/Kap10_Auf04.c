/**
@file: Kap10_Auf04.c
@brief: Contains main program
@author: Mike
@date: 10.12.2017
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct test
{
   char acString[FILENAME_MAX];
};
/**
@fn void main(void)
@brief Print Input to Textfile
@param
@return
@author mike
@date 12/13/17
*/
void main(void)
{
   struct test structtest[10];
   int iStructSize = ((sizeof(structtest)) / (sizeof(struct test)));

   FILE *pf;
   char acFilename[] = "c:\\temp\\err.txt";
   printf("Strings that starts with 'e' can be written to %s\n", acFilename);
   pf = fopen(acFilename, "w+");
   for (int i = 0; i <= iStructSize; i++) {
      printf("Value:\n");
      scanf("%s", structtest[i].acString);
   }

   for (int j = 0; j <= iStructSize; j++) {
      if (pf != NULL && ((strncmp("e", structtest[j].acString, 1) == 0) || (strncmp("E", structtest[j].acString, 1) == 0))) {
         fprintf(pf, "%s\n", structtest[j].acString);
      }
   }
   fclose(pf);
}
