/**
@file: Kap04_Auf05.c
@brief: Contains main program
@author: Mike
@date: 03.11.2017
*/
#include <stdio.h>
#include <conio.h>

/**
@fn main(void)
@brief Bit operations
@param void
@return
@author Mike
@date 03.11.2017
*/

int main(void)
{
   unsigned short int usiTest1 = 5;
   unsigned short int usiTest2 = 256;
   usiTest1 = ~usiTest1;
   printf("%u\n", usiTest1);
   usiTest2 = usiTest2 >> 1;
   printf("%u\n", usiTest2);

   _getch();
}