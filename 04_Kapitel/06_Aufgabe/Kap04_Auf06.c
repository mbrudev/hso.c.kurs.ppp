/**
@file: Kap04_Auf06.c
@brief: Contains main program
@author: Mike
@date: 03.11.2017
*/

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

/**
@fn main(void)
@brief Bitwise operations
@param void
@return EXIT_SUCCESS
@author Mike
@date 03.11.2017
*/

int main(void)
{
   unsigned short int usiWert1;
   unsigned short int usiWert2;
   unsigned short int usiWert3;

   usiWert1 = 0x0A4D; //‭0000 1010 0100 1101‬ = ‭2637‬
   usiWert2 = 0x30F1; //‭0011 0000 1111 0001‬ = ‭12529‬

   usiWert3 = usiWert1 & usiWert2; //0000 0000 0100 0001 = 0x‭41‬
   printf("usiWert1 & usiWert2 = usiWert3: %X\n", usiWert3);

   usiWert3 = usiWert2 | 0x000A; //11000011111011 = 0x30FB
   printf("usiWert2 | 0x000A = usiWert3: %X\n", usiWert3);

   usiWert3 = usiWert1 ^ usiWert2;  //0011 1010 1011 1100 =0x3ABC
   printf("usiWert1 ^ usiWert2 = usiWert3: %X\n", usiWert3);

   usiWert2 = ~usiWert2; //1100 1111 0000 1110 = 0xCF0E
   printf("~usiWert2 = usiWert2: %X\n", usiWert2);

   usiWert3 = usiWert1 >> 2; //12529/2^2 = 0x293
   printf("usiWert1 >> 2 = usiWert3: %X\n", usiWert3);

   usiWert3 = usiWert1 << 2; //12529*2^2 = 0x2934
   printf("usiWert1 << 2 = usiWert3: %X\n", usiWert3);


   _getch();
   return EXIT_SUCCESS;
}