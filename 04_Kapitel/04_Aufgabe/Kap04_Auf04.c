/**
@file: Kap04_Auf04.c
@brief: Contains main program
@author: Mike
@date: 03.11.2017
*/
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <math.h>

/**
@fn main(void)
@brief Calculates spherical volume
@param void
@return EXIT_SUCCESS (0)
@author Mike
@date 03.11.2017
*/

int main(void)
{

   const float cfPi = 3.14159265359F;
   float fRadius;
   float fVolume;

   printf("Enter a Number to calculate spherical volume\n");

   scanf_s("%f", &fRadius);
   printf("Your input was: %f\n", fRadius);

   fVolume = (4 * cfPi * powf(fRadius, 3)) / 3; //Calculates spherical volume
   printf("Spherical volume = %f", fVolume);

   _getch();
   return EXIT_SUCCESS;
}