/**
@file: main.c
@brief: Contains main program
@author: Mike
@date: 03.11.2017
*/
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

/**
@fn main(void)
@brief C language operators
@param void
@return EXIT_SUCCESS (0)
@author Mike
@date 03.11.2017
*/

int main(void)
{
   int iA = 5;
   int iB =10;
   int iResult;

   //int iA = 5
   //int iB = 9
   //int iResult iA + iB = 14

   //int iA = 10
   //int iB = 7
   //int iResult iA - iB = 3

   //int iA = 5
   //int iB = 9
   //int iResult = iA / iB = 0

   //int iA = 12
   //int iB = 5
   //int iResult = iA / iB = 1

   //int iA = 8
   //int iB = 7
   //int iResult = iA * iB = 56

   //int iA = 11
   //int iB = 3
   //int iResult = iA % iB = 2

   //int iA = 5
   //int iB =
   //int iResult = 5 (iA++) = 5

   //int iA = 5
   //int iB =
   //int iResult = 5 (++iA) = 6

   //int iA =
   //int iB = 10
   //int iResult = 10 (iB--) = 10

   //int iA =
   //int iB = 10
   //int iResult = 10 (--iB) = 9



   iResult = --iB;

   printf("%i", iResult);
   _getch();

   return EXIT_SUCCESS;
}