/**
@file: Kap04_Auf03.c
@brief: Contains main program
@author: Mike
@date: 03.11.2017
*/
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <math.h>

/**
@fn main(void)
@brief Calcualtes Spherical Volume
@param void
@return EXIT_SUCCESS (0)
@author Mike
@date 03.11.2017
*/

int main(void)
{

   const float cfPi = 3.14159265359F;
   float fRadius = 1;
   float fVolume;

   
/* This Section is needed for 04_Aufgabe
   printf("Enter a Number to calculate spherical volume\n");

   scanf_s("%f", &fRadius);
   printf("Your input was: %f\n", fRadius);
*/

   fVolume = (4 * cfPi * powf(fRadius, 3)) / 3; // Calculates spherical volume
   printf("Spherical volume for Radius = 1: = %f", fVolume);

   _getch();
   return EXIT_SUCCESS;
}