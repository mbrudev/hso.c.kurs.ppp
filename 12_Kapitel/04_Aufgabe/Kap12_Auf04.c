/**
@file: Kap12_Auf04.c
@brief: Contains main program
@author: Mike
@date: 17.12.2017
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 3600
#define ROW 100

double *bubblesort(double *pd);
double *fStrtok(FILE *pf, char acDelimiter[]);
void fOutput(FILE *pf, double *pd);

/**
@fn int main(void)
@brief main function
@param void
@return EXIT_SUCCESS (0)
@author Mike
@date 17.12.2017
*/
int main(void)
{
   FILE *pfInput, *pfOut;
   char acInputFile[] = "zahlen1.txt";
   char acOutFile[] = "zahlen2.txt";
   char acDelimiter[] = ";,";

   pfInput = fopen(acInputFile, "r");
   pfOut = fopen(acOutFile, "w+");

   double *pdVal = fStrtok(pfInput, acDelimiter);
   double *pdBubble = bubblesort(pdVal);
   fOutput(pfOut, pdBubble);

   return EXIT_SUCCESS;
}

/**
@fn void fOutput(double *pd)
@brief Print Output
@param double *pd
@return
@author Mike
@date 17.12.2017
*/
void fOutput(FILE *pf, double *pd)
{
   double adMatrix[ROW][(SIZE / ROW)];
   for (int j = 0; j < (ROW); j++) {
      for (int i = 0; i < (SIZE / ROW); i++) {
         adMatrix[i][j] = *pd;
         pd++;
         fprintf(pf, "%.2lf ", adMatrix[i][j]);
      }
      fprintf(pf, "\n");
   }
}

/**
@fn double *fStrtok(FILE *pf, char acDelimiter[])
@brief Splits input values ​​based on a given separator
@param FILE *pf, char acDelimiter[]
@return double sdValues
@author Mike
@date 17.12.2017
*/
double *fStrtok(FILE *pf, char acDelimiter[])
{
   int i = 0;
   char acLine[3600];
   char *pcToken;
   static double sdValues[SIZE];

   if (pf != NULL) {
      while (feof(pf) == NULL) {
         fgets(acLine, sizeof(acLine), pf); // file gets
         pcToken = strtok(acLine, acDelimiter);
         while (pcToken != NULL) {
            sdValues[i] = atof(pcToken);
//            printf("%lf\n", sdValues[i]);
            pcToken = strtok(NULL, acDelimiter);
            i++;
         }
      }
      fclose(pf);
   }
   return sdValues;
}

/**
@fn double *bubblesort(int *pd)
@brief sort array via buublesort
@param double *pd
@return pd
@author Mike
@date 17.12.2017
*/
double *bubblesort(double *pd)
{
   double dTemp;
   for (int i = 0; i < SIZE; i++) {
      for (int j = 0; j < SIZE; j++) {
         if (pd[i] < pd[j]) {
            dTemp = pd[i];
            pd[i] = pd[j];
            pd[j] = dTemp;
         }
      }
   }
   return pd;
}

