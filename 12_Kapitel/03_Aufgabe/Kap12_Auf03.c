#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

#define DOSHOWOUTPUT 1

int main(void)
{
   printf("Hello World!\n");

   #if DOSHOWOUTPUT
   printf("Hello user!\n");
   #endif

   // Wait for user interaction
   _getch();

   return EXIT_SUCCESS;
}
