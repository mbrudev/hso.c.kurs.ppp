/**
@fn int main(int argc, char *argv[])
@brief contains main function
@param int argc, char *argv[]
@return EXIT_SUCCESS (0)
@author mike
@date 12/20/17
*/
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char *argv[])
{
   int i;
   for (i = 0; i < argc; i++) {
      printf("\n%d %s", i, argv[i]);
   }
   return EXIT_SUCCESS;
}
