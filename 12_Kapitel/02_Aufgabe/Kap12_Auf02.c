/**
@file: Kap12_Auf02.c
@brief: Contains main program
@author: Mike
@date: 15.12.2017
*/
#include <stdio.h>
#include <stdlib.h>
enum triangle
{
   sideA = 0,
   sideB = 1,
   sideC = 2
};

static short int eTriangleElements = 1 + (sideC - sideA);

void checkTriangle(double adValues[], int iSize);
void disabilityCheck(double adValues[], short iSize);
/**
@fn int main(int argc, char *argv[])
@brief check plausibility of triangle
@param int argc char *argv[]
@return EXIT_SUCCESS (0)
@author Mike
@date 15.12.2017
*/
int main(int argc, char *argv[])
{
   short int iArgcSize = (short) (argc - 1);
   double adArgs[eTriangleElements];

   if (iArgcSize == 0) {
      printf("");
   }
   else if (iArgcSize > eTriangleElements) {
      printf("Too many values: %i.\nPlease insert %i values", iArgcSize, eTriangleElements);
   }
   else if (iArgcSize < eTriangleElements) {
      for (int i = 0; i < 2; i++) {
         adArgs[i] = atof(argv[(i + 1)]);
      }
      disabilityCheck(adArgs, 2);
   }
   else {
      for (int i = 0; i < eTriangleElements; i++) {
         adArgs[i] = atof(argv[(i + 1)]);
      }
      checkTriangle(adArgs, eTriangleElements);
   }
   return EXIT_SUCCESS;
}

/**
@fn void disabilityCheck(double adValues[], short iSize)
@brief Check if user is stupid
@param (double adValues[], short iSize)
@return
@author Mike
@date 15.12.2017
*/
void disabilityCheck(double adValues[], short iSize)
{
   if (((iSize == 2) && (adValues[sideA] != 0) && (adValues[sideA] == adValues[sideB]))) {
      printf("Triangle a(%.2lf) b(%.2lf) is isosceles ?\n",
             adValues[sideA],
             adValues[sideB]);
   }
   else {
      printf("Less values a(%.2lf) b(%.2lf) \nPlease insert %i values",
             adValues[sideA],
             adValues[sideB],
             eTriangleElements);
   }
}

/**
@fn void checkTriangle(double adValues[], int iSize)
@brief check type of triangle
@param double adValues[] int iSize
@return
@author Mike
@date 15.12.2017
*/
void checkTriangle(double adValues[], int iSize)
{
   if (((adValues[sideA] == 0 || adValues[sideB] == 0 || adValues[sideC] == 0))) {
      printf("Triangle a(%.2lf) b(%.2lf) c(%.2lf) is not valid\n", adValues[sideA], adValues[sideB], adValues[sideC]);
   }
   else {
      if ((adValues[sideA] == adValues[sideB]) && (adValues[sideB] == adValues[sideC])) {
         printf("Triangle a(%.2lf) b(%.2lf) c(%.2lf) is equilateral\n",
                adValues[sideA],
                adValues[sideB],
                adValues[sideC]);
      }
      else if ((adValues[sideA] == adValues[sideB]) || (adValues[sideA] == adValues[sideC])
          || (adValues[sideB] == adValues[sideC])) {
         printf("Triangle a(%.2lf) b(%.2lf) c(%.2lf) is isosceles\n",
                adValues[sideA],
                adValues[sideB],
                adValues[sideC]);
      }
      else {
         printf("Triangle a(%.2lf) b(%.2lf) c(%.2lf) is valid\n", adValues[sideA], adValues[sideB], adValues[sideC]);
      }
   }
}

