/**
@file: main.c
@brief: Contains main program
@author: mike
@date: 15.11.17
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void userInput(void);
double quadEquation(double a, double b, double c);

/**
@fn int main(void)
@brief Call userInput()
@param void
@return EXIT_SUCCESS (0);
@author mike
@date 15.11.17
*/
int main(void)
{
   userInput();
   return EXIT_SUCCESS;
}

/**
@fn double quadEquation(double a, double b, double c)
@brief Calculates quadratic equations
@param double a, double b, double c
@return EXIT_SUCCESS (0)
@author mike
@date 15.11.17
*/
double quadEquation(double a, double b, double c)
{
   double dDiscriminant = ((pow(b, 2)) - 4 * a * c);
   if (dDiscriminant < 0) {
      printf("Square root(%lf) is a complex number\nNO RESULT", dDiscriminant);
   }
   else {
      double dX1 = ((-b) + sqrt(dDiscriminant)) / 2 * a;
      double dX2 = ((-b) - sqrt(dDiscriminant)) / 2 * a;
      if (dX1 != dX2) {
         printf("x1 = %lf\nx2 = %lf\n", dX1, dX2);
      }
      else {
         printf("x1 = %lf\n", dX1);
      }
   }
   return EXIT_SUCCESS;
}

/**
@fn void userInput(void)
@brief read user input
@param void
@return
@author mike
@date 15.11.17
*/
void userInput(void)
{
   double a, b, c;
   a = 1;
   printf("Enter two numbers to calculate quadratic equation\n");
   scanf("%lf %lf", &b, &c);
   quadEquation(a, b, c);
}
