/**
@file: print_Status.c
@brief: Contains print_Status
@author: Mike
@date: 18.11.2017
*/

#include <stdio.h>
#include <stdlib.h>


void printSeperator();
void statusLayout(char cKeyVal)
{
   printSeperator();
   printf("[Status] Key %c was pressed\n", cKeyVal);
   printSeperator();
}
