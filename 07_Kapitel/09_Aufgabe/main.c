/**
@file: Kap07_Auf09a.c
@brief: Contains main program
@author: Mike
@date: 18.11.2017
*/
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

int print_menu();
void statusLayout(char cKeyVal);

int main(void)
{
   char iInput = 0;
   char iRetVal = -1;

   while (1) {
      print_menu();
      iInput = (char) getchar();

      switch (tolower(iInput)) {
         case 'a':
         case 'b':
         case 'r':
         case 's': statusLayout(iInput);
            system("pause");
//            _clrscr();
            break;
         default:printf("No valid Key\n");
            system("pause");
//            _clrscr();
            break;
      }
   }
   return EXIT_SUCCESS;
}