/**
@file: print_Menu.c
@brief: Contains print_Menu
@author: Mike
@date: 18.11.2017
*/

#include <stdio.h>
#include <stdlib.h>
#include "include_Header/utilities.h"

void printSeperator();
int menuLayout();

int print_menu()
{
   char acTitle[] = "Menu";
   printSeperator();
   printf("%s\n", acTitle);
   printSeperator();
   menuLayout();
   return EXIT_SUCCESS;
}

void printSeperator(void)
{
   for (int i = 0; i < 50; i++) {
      printf("-");
   }
   printf("\n");
}

int menuLayout()
{
   char *cpArray[4] = {"-b- (Beginn)", "-r- (Reset)", "-s- (Stop)", "-a- (Abbruch)"};

   for (int i = 0; i < ((sizeof(cpArray)) / (sizeof(char *))); i++) {

      if ((i + 2) % 2 == 0) {
         _gotoxy(0, ((short) (i + 2))); // x = 0 y = 2 Beginn 0 + 2 = 2
         printf("\n%s", cpArray[i]);  // x = 0 y = 4  Beginn 2 + 2 = 4
      }
      else {
         _gotoxy(sizeof(cpArray), ((short) (i + 2)));
         printf("%s\n", cpArray[i]);
      }

   }
   printSeperator();
   return EXIT_SUCCESS;
}
