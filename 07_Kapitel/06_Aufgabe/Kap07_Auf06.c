/**
@file: Kap07_Auf06.c
@brief: Contains main program
@author: mike
@date: 16.11.17
*/
#include <stdio.h>
#include <stdlib.h>

void userInput(void);
int facRec(int iFacRec);
int fac(int iFac);

/**
@fn int main(void)
@brief main function
@param void
@return EXIT_SUCCESS (0)
@author mike
@date 16.11.17
*/
int main(void)
{
   userInput();
   return EXIT_SUCCESS;
}

/**
@fn void userInput(void)
@brief User input
@param void
@return
@author mike
@date 16.11.17
*/
void userInput(void)
{
   int i;
   printf("Enter a Number for FakRek\n");
   scanf_s("%i", &i);
   facRec(i);
   printf("facRec: %i\n", facRec(i));

   printf("Enter a new Number for Fak\n");
   scanf_s("%i", &i);
   fac(i);
   printf("fak: %i\n", fac(i));

}
/**
@fn int fakRek(int iFakRek)
@brief Recursive Function
@param iFacRec
@return fakRek(iFakRek - 1) * iFakRek
@author mike
@date 16.11.17
*/
int facRec(int iFacRec)
{
   int i;
   if (iFacRec <= 1) {
      i = 1;
   }
   else {
      i = facRec(iFacRec - 1) * iFacRec;
   }
   return i;
}

/**
@fn int fak(int iFak)
@brief
@param iFac
@return iFakResult
@author mike
@date 16.11.17
*/
int fac(int iFac)
{
   int iFakResult = 1;
   for (int i = 2; i <= iFac; ++i) {
      iFakResult *= i;
   }
   return iFakResult;
}