/**
@file: Kap07_Auf01.c
@brief: Contains main program
@author: mike
@date: 15.11.17
*/
#include <stdio.h>
#include <stdlib.h>

/**
@fn int main()
@brief main with for loop
@param void
@return EXIT_SUCCESS (0)
@author mike
@date 15.11.17
*/
int main(void)
{
   for (int i = 1; i < 11; ++i)
   {
      if (i == 3)
      {
         printf("Three: %i\n", i);
      }
      else if (i == 7)
      {
         printf("Seven: %i\n", i);
      }
      else
      {
         printf("%i\n", i);
      }
   }
   return EXIT_SUCCESS;
}