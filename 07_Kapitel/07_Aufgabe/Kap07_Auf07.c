/**
@file: Kap07_Auf07.c
@brief: Contains main program
@author: mike
@date: 16.11.17
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


const short int cusiMarkSquare = 5;

static int *cSieve;

static int uiInput;

static int uiInputLim;


void printPrimes(void);

/**
@fn int main(void)
@brief main function
@param void
@return EXIT_SUCCESS (0)
@author mike
@date 16.11.17
*/
int main(void)
{

   static int siNonPrimes;
   printf("Enter a number to calculate primes\n");

   scanf("%i", &uiInput);

   cSieve = malloc((uiInput * sizeof(int)));
   printf("%p\n", cSieve);

   uiInputLim = (int) sqrt(uiInput);

   for (int i = 1; i <= uiInputLim; i++) {
      for (int j = 0; j <= uiInputLim; j++) {
         siNonPrimes = (4 * i * i) + (j * j);

         if (siNonPrimes <= uiInput && (siNonPrimes % 12 == 1) || (siNonPrimes % 12 == 5)) {
            cSieve[siNonPrimes] = !cSieve[siNonPrimes];
         }

         siNonPrimes = (3 * i * i) + (j * j);
         if (siNonPrimes <= uiInput && (siNonPrimes % 12 == 7)) {
            cSieve[siNonPrimes] = !cSieve[siNonPrimes];
         }

         siNonPrimes = (3 * i * i) - (j * j);
         if (i > j && siNonPrimes <= uiInput && (siNonPrimes % 12 == 11)) {
            cSieve[siNonPrimes] = !cSieve[siNonPrimes];
         }
      }//end for j
   }// end for i

   for (int iOutput = cusiMarkSquare; iOutput * iOutput < uiInputLim; iOutput++) {
      if (cSieve[iOutput]) {
         for (int i = iOutput * iOutput; i < uiInputLim; i += iOutput * iOutput) {
            cSieve[i] = !cSieve;
         }
      }
   }
   printPrimes();
   return EXIT_SUCCESS;
}

/**
@fn void printPrimes(void)
@brief print Primes function
@param void
@return
@author mike
@date 16.11.17
*/
void printPrimes(void)
{
   printf("Output primes\n2\n3\n");
   for (int iOutput = cusiMarkSquare; iOutput < uiInput; iOutput++) {
      if (cSieve[iOutput]) {
         printf("%i\n", iOutput);
      }
   }
}