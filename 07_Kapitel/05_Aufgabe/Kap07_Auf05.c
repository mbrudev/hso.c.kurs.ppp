/**
@file: Kap07_Auf05.c
@brief: Contains main program
@author: Mike
@date: 15.11.2017
*/

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

/**
@fn int main(void)
@brief Validate if is a number or character
@param void
@return iRetVal
@author Mike
@date 18.11.2017
*/
int main(void)
{
   char const ccExit = 'e';
   char iRetVal = (-1);
   char iInput;

   while (iRetVal != ccExit) {
      printf("Press any Key\n");
      iInput = (char) _getch();

      switch (iInput) {
         case '0':
         case '1':
         case '2':
         case '3':
         case '4':
         case '5':
         case '6':
         case '7':
         case '8':
         case '9':printf("%c is a number\n", iInput);
            break;

         default:printf("Input was a character\n");
            break;
      }
      iRetVal = iInput;
   }
   return iRetVal;
}



