/**
@file: Kap07_Auf03.c
@brief: Contains main program
@author: mike
@date: 15.11.17
*/
#include <stdio.h>
#include <stdlib.h>

/**
@fn int main(
@brief output numbers % 19
@param void
@return EXIT_SUCCESS (0)
@author mike
@date 15.11.17
*/

int main()
{
   for (int i = 0; i <= 1000; ++i)
   {
      if (i % 19 == 0)
      {
         printf("number / 19: %i\n", i);
      }
   }
   return EXIT_SUCCESS;
}