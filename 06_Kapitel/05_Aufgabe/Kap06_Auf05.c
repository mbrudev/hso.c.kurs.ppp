/**
@file: Kap06_Auf05.c
@brief: Contains main program
@author: Mike
@date: 11.11.2017
*/
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

/**
@fn int main()
@brief main function
@param void
@return EXIT_SUCCESS (0)
@author Mike
@date 11.11.2017
*/

int uselessFunction(int i); // First Declaration of int i.

int main()
{
   for (int i = 0; i < 2; ++i) // Second Declaration and Initialization of int i.
   {
      printf("%i\n", i);
   }

   int i = 3; // Third Declaration and Initialization of int i.

   uselessFunction(i);
   _getch();
}

/**
@fn int uselessFunction(int i)
@brief prints int i in different ways
@param int i
@return int i
@author Mike
@date 11.11.2017
*/
int uselessFunction(int i)
{
   {
      int i = 2; // Fourth Declaration and Initialization of int i.
      printf("%i\n", i);
   }
   printf("%i\n", i);
   return i;
}