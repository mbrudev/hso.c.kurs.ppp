cmake_minimum_required(VERSION 3.8)
project(05_Aufgabe)

set(CMAKE_C_STANDARD 99)

set(SOURCE_FILES Kap06_Auf05.c)
add_executable(05_Aufgabe ${SOURCE_FILES})