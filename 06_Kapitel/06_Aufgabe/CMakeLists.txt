cmake_minimum_required(VERSION 3.8)
project(06_Aufgabe)

set(CMAKE_C_STANDARD 99)

set(SOURCE_FILES Kap06_Auf06a.c Kap06_Auf06b.h Kap06_Auf06c.h)
add_executable(06_Aufgabe ${SOURCE_FILES})