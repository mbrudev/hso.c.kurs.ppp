/**
@file Kap06_Auf06b.h
@brief Contains function f1
@author Mike 
@date 12.11.2017
*/

/**
@fn void f1(void)
@brief Function f1
@param void
@return
@author Mike
@date 12.11.2017
*/
void f1(void)
{
   int iA; // auto int iA;
   printf("random value = %i\n", iA);
   iA = 10;
   printf("%i\n", iA);
}