/**
@file: Kap06_Auf06a.c
@brief: Contains main program
@author: Mike
@date: 12.11.2017
*/

#include <stdio.h>
#include <stdlib.h>
#include "Kap06_Auf06b.h"
#include "Kap06_Auf06c.h"

/**
@fn int main(void)
@brief main function
@param void
@return EXIT_SUCCESS (0)
@author Mike
@date 12.11.2017
*/

int main(void)
{
   f1();
   f2();

   return EXIT_SUCCESS;
}