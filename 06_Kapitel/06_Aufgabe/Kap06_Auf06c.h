/**
@file Kap06_Auf06c.h
@brief Contains function f2
@author Mike 
@date 12.11.2017
*/

/**
@fn void f2(void)
@brief Function f2
@param void
@return
@author Mike
@date 12.11.2017
*/
void f2(void)
{
   static int iB; // auto int iA;
   printf("%i\n", iB);
   iB = 10;
   printf("%i\n", iB);

}
