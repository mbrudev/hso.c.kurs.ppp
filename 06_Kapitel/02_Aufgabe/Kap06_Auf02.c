/**
@file: Kap06_Auf02.c
@brief: Contains main program
@author: Mike
@date: 11.11.2017
*/

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

/**
@fn int main()
@brief main function
@param void
@return EXIT_SUCCESS (0)
@author Mike
@date 11.11.2017
*/

int main()
{
   int iDecimalVal;     //Value 47
   int iOctalVal;       //Value 57
   int iHexadecimalVal; //Value 2F

   printf("Enter a Decimal Number, Octal Number, Hexadecimal Number\n");
   scanf_s("%i %o %x", &iDecimalVal, &iOctalVal, &iHexadecimalVal);
   printf("Decimal: %i \nOctal: %o \nHex: %X\n", iDecimalVal, iOctalVal, iHexadecimalVal);

   _getch();
   return EXIT_SUCCESS;
}