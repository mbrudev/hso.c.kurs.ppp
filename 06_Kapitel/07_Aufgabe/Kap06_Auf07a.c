/**
@file: Kap06_Auf07a.c
@brief: Contains main program
@author: Mike
@date: 12.11.2017
*/
#include <stdio.h>
#include <stdlib.h>

int iGlobal1;
static int iGlobal2;
extern int iGlobal3;

void f11(int iCount);
void f12(int iCount);
void f21(void);
void f22(int iCount);

/**
@fn int main(void)
@brief main function
@param void
@return EXIT_SUCCESS (0)
@author Mike
@date 12.11.2017
*/
int main(void)
{
   f11(11);
   f12(12);
   f21();
   f22(22);

   return EXIT_SUCCESS;
}

/**
@fn void f11(int iCount)
@brief Print variables
@param iCount
@return
@author Mike
@date 12.11.2017
*/

void f11(int iCount)
{
   int iA = iCount;
   printf("%i\n", iA);

   iA = iGlobal1;
   printf("iGlobal1: %i\n", iA);

   iGlobal2 = 5;
   printf("iGlobal2: %i\n", iGlobal2);

}

/**
@fn void f12(int iCount)
@brief Print variables
@param iCount
@return
@author Mike
@date 12.11.2017
*/

void f12(int iCount)
{
   iGlobal2 = iCount;
   printf("iGlobal2: %i\n", iGlobal2);
   printf("iGlobal3: %i\n",iGlobal3);
}