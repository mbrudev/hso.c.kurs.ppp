/**
@file Kap06_auf07b.c
@brief
@author Mike 
@date 12.11.2017
*/

#include <stdio.h>

extern int iGlobal1;
static int iGlobal2;
int iGlobal3 = 3;

/**
@fn void f21(void)
@brief Print variables
@param void
@return
@author Mike
@date 12.11.2017
*/

void f21(void)
{
   iGlobal1 = 55;
   printf("iGlobal1: %i\n", iGlobal1);
}

/**
@fn void f22(int iCount)
@brief Print variables
@param iCount
@return
@author Mike
@date 12.11.2017
*/

void f22(int iCount)
{
   iGlobal2 = iCount;
   printf("iGlobal2 %i\n", iGlobal2);
   iGlobal3 = (3+3);
   printf("iGlobal3: %i", iGlobal3);
}