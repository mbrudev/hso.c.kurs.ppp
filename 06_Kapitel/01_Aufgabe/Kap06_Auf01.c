/**
@file: Kap06_Auf01.c
@brief: Contains main program
@author: Mike
@date: 11.11.2017
*/

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

/**
@fn int main()
@brief main function
@param void
@return EXIT_SUCCESS (0)
@author Mike
@date 11.11.2017
*/
int main()
{
   char c;
   int i;
   int usi;
   float f;
   double d;
   char acName[50];

   printf("char\n");
   c = _getch();
   printf("%c\n", c);

   printf("integer\n");
   scanf_s("%i",&i);
   printf("input integer = %i\n", i);

   printf("unsigned integer\n");
   scanf_s("%usi",&usi);
   printf("input unsigned integer = %i\n", usi);

   printf("float\n");
   scanf_s("%f",&f);
   printf("input float = %lf\n", f);

   printf("double\n");
   scanf_s("%lf",&d);
   printf("input double = %lf\n", d);

   printf("char length 50\n");
   scanf_s("%s", &acName, 50);
   printf("input char[50} = %s \n", acName);

   _getch();
   return EXIT_SUCCESS;
}