/**
@file: Kap08_Auf01.c
@brief: Contains main program
@author: Mike
@date: 25.11.2017
*/

#include <stdio.h>
#include <stdlib.h>

/**
@fn int main(void)
@brief Print Array
@param void
@return EXIT_SUCCESS (0)
@author Mike
@date 25.11.2017
*/
int main(void)
{
   char acFirstName[20];
   acFirstName[0] = 'm';
   acFirstName[1] = 'i';
   acFirstName[2] = 'k';
   acFirstName[3] = 'e';

   printf("%s", acFirstName);

   return EXIT_SUCCESS;
}