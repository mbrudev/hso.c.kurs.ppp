/**
@file: Kap08_Auf06.c
@brief: Contains main program
@author: Mike
@date: 26.11.2017
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

union twist
{
   char cA;
   unsigned int uiB;
} uTwist;

/**
@fn int main(void)
@brief print union
@param void
@return EXIT_SUCCESS (0)
@author Mike
@date 26.11.2017
*/
int main(void)
{

   uTwist.uiB = 1;
   printf("Print 1: %u\n", uTwist.uiB); // Output == 1

   uTwist.cA = 'A';
   printf("Print 2: %u\n", uTwist.uiB); // Print ASCII Value for 'A' Output == 65

   printf("size of union: %d\n", sizeof(union twist));
   system("pause");

   return EXIT_SUCCESS;
}