/**
@file: Kap08_Auf05.c
@brief: Contains main program
@author: Mike
@date: 25.11.2017
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>

/**
@fn int main(void)
@brief Struct of employee
@param void
@return EXIT_SUCCESS (0)
@author Mike
@date 25.11.2017
*/

enum department
{
   humanResources,
   development,
   production,
   management
};

struct address
{
   unsigned int uiZipcode;
   char acCity[50];
   char acStreet[50];
   char acHnr[10];
};

struct employee
{
   char acLastName[50];
   char acFirstName[50];
   unsigned int usiAge;
   struct address sStructaddress;
   enum department eDepartment;
};

int main(void)
{


   struct employee structemployee[3];
   int iSS = ((sizeof(structemployee)) / (sizeof(struct employee)));

   char *cp[3]; // TODO init *cp with malloc

   for (int i = 0; i < iSS; i++)
   {
      printf("First Name: ");
      scanf("%s", structemployee[i].acFirstName);
      printf("Last Name: ");
      scanf("%s", structemployee[i].acLastName);
      printf("Age: ");
      scanf("%u", &structemployee[i].usiAge);

      printf(
          "Department:\nFor Human Resources press %d\nFor Development press %d\nFor Production press %d\nFor Management press %d\n",
          humanResources,
          development,
          production,
          management);
      scanf("%d", &structemployee[i].eDepartment);

      switch (structemployee[i].eDepartment) {
         case humanResources: cp[i] = "Human Resources";
            break;
         case development: cp[i] = "Development";
            break;
         case production: cp[i] = "Production";
            break;
         case management: cp[i] = "Management";
            break;
         default: cp[i] = "No Department set";
            break;
      }
      printf("Zipcode: \n");
      scanf("%u", &structemployee[i].sStructaddress.uiZipcode);
      printf("City: \n");
      scanf("%s", structemployee[i].sStructaddress.acCity);
      printf("Street: \n");
      scanf("%s", structemployee[i].sStructaddress.acStreet);
      printf("Number: \n");
      scanf("%s", structemployee[i].sStructaddress.acHnr);
   }

//   Output
   for (int j = 0; j < iSS; j++)
   {
      printf("First Name: %s\n", structemployee[j].acFirstName);
      printf("Last Name: %s\n", structemployee[j].acLastName);
      printf("Age: %i\n", structemployee[j].usiAge);

      printf("Department: %s\n", cp[j]);
      printf("Zipcode: %u\n", structemployee[j].sStructaddress.uiZipcode);
      printf("City: %s\n", structemployee[j].sStructaddress.acCity);
      printf("Street: %s\n", structemployee[j].sStructaddress.acStreet);
      printf("House Number: %s\n", structemployee[j].sStructaddress.acHnr);

      printf("sizeof department = %d\n", sizeof(enum department));
      printf("sizeof employee = %d\n", sizeof(struct employee));
      printf("sizeof address = %d\n", sizeof(struct address));
   }

   _getch();
   return EXIT_SUCCESS;
}