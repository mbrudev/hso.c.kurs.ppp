/**
@file: Kap08_Auf07.c
@brief: Contains main program
@author: Mike
@date: 26.11.2017
*/

#include <stdio.h>
#include <stdlib.h>

/**
@fn int main(void)
@brief print bitfield
@param void
@return EXIT_SUCCESS (0)
@author Mike
@date 26.11.2017
*/

struct bitfield
{
   unsigned int bit1: 1;
   unsigned int bit2: 2;
   unsigned int bit3: 3;
   unsigned int bit4: 4;
   unsigned int bit5: 5;
};


int main(void)
{

   struct bitfield sBitfield;

   sBitfield.bit1 = 1;
   sBitfield.bit2 = 3;
   sBitfield.bit3 = 7;
   sBitfield.bit4 = 15;
   sBitfield.bit5 = 31;

   printf("%u\n%u\n%u\n%u\n%u\n", sBitfield.bit1, sBitfield.bit2, sBitfield.bit3, sBitfield.bit4, sBitfield.bit5);

   return EXIT_SUCCESS;
}