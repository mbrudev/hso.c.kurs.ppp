/**
@file: Kap08_Auf08.c
@brief: Contains main program
@author: Mike
@date: 26.11.2017
*/

#include <stdio.h>
#include <stdlib.h>

/**
@fn int main(void)
@brief print size of data type
@param void
@return EXIT_SUCCESS (0)
@author Mike
@date 26.11.2017
*/

int main(void)
{
   short int si;
   int i;
   unsigned int ui;
   long int li;
   float f;
   double d;
   char c;

   printf("sizeof(short int): %d\n", sizeof(si));
   printf("sizeof(int): %d\n", sizeof(i));
   printf("sizeof(unsigned int): %u\n", sizeof(ui));
   printf("sizeof(long int): %u\n", sizeof(li));
   printf("sizeof(float): %d\n", sizeof(f));
   printf("sizeof(double): %d\n", sizeof(d));
   printf("sizeof(char): %d\n", sizeof(c));
   printf("----------------------------------------------\n");
   printf("sizeof(short int): %d\n", sizeof(short int));
   printf("sizeof(int): %d\n", sizeof(int));
   printf("sizeof(unsigned int): %u\n", sizeof(unsigned int));
   printf("sizeof(long int): %u\n", sizeof(long int));
   printf("sizeof(float): %d\n", sizeof(float));
   printf("sizeof(double): %d\n", sizeof(double));
   printf("sizeof(char): %d\n", sizeof(char));

   return EXIT_SUCCESS;
}