cmake_minimum_required(VERSION 3.8)
project(08_Aufgabe)

set(CMAKE_C_STANDARD 99)

set(SOURCE_FILES Kap08_Auf08.c)
add_executable(08_Aufgabe ${SOURCE_FILES})