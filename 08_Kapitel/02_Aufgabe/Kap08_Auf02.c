/**
@file: Kap08_Auf02.c
@brief: Contains main program
@author: mike
@date: 11/23/17
*/
#include <stdio.h>
#include <math.h>

/**
@fn int main(void)
@brief Calculate Voltage
@param void
@return
@author mike
@date 11/23/17
*/
int main(void)
{

   double dU1 = 10;
   double dU2[7];
   double dR1, dR2, dRtot;
   double iR3[] = {pow(10, -9), pow(10, 1), pow(10, 2), pow(10, 3), pow(10, 4), pow(10, 5), pow(10, 6)};
   double dI;

   dR1 = dR2 = 100; //Ohm

   for (int i = 0; i < ((sizeof(iR3)) / (sizeof(double *))); i++) {
      dRtot = (1 / ((1 / dR2) + (1 / iR3[i])) + dR1);
      dI = (dU1 / dRtot);

      dU2[i] = (dRtot -dR1) * dI;
      printf("Result U2::%lf\n", dU2[i]);

   }
}