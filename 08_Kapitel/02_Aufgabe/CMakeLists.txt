cmake_minimum_required(VERSION 3.8)
project(01_Aufgabe)

set(CMAKE_C_STANDARD 99)

set(SOURCE_FILES Kap08_Auf02.c)
add_executable(01_Aufgabe ${SOURCE_FILES})