/**
@file: Kap08_Auf03.c
@brief: Contains main program
@author: Mike
@date: 25.11.2017
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>

/**
@fn int main(void)
@brief Struct of employee
@param void
@return EXIT_SUCCESS (0)
@author Mike
@date 25.11.2017
*/


struct employee
{
   char acLastName[50];
   char acFirstName[50];
   int usiAge;
};

int main(void)
{
   struct employee structemployee[3];
   int iSS = ((sizeof(structemployee)) / (sizeof(struct employee)));

   for (int i = 0; i < iSS; i++) {
      scanf("%s", structemployee[i].acFirstName);
      scanf("%s", structemployee[i].acLastName);
      scanf("%d", &structemployee[i].usiAge);
   }

   for (int j = 0; j < iSS; j++) {
      printf("%s\n", structemployee[j].acFirstName);
      printf("%s\n", structemployee[j].acLastName);
      printf("%i\n", structemployee[j].usiAge);
      printf("sizeof = %d\n", sizeof(struct employee));
   }

   _getch();

   return EXIT_SUCCESS;
}