/**
@file: Kap13_Auf03.c
@brief: Contains main program
@author: Mike
@date: 16.12.2017
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void fStrtok(FILE *pFILE, char acDelimiter[]);
void fStrlen(char ac[]);
void fstrlwr(char ac[]);
void fstrupr(char ac[]);
void fStrstr(char ac[], char *pcNeedle);

/**
@fn void main(void)
@brief main function
@param void
@return
@author Mike
@date 16.12.2017
*/
void main(void)
{
   FILE *pf;
   char *pcExtension = ".csv";
   char cFilename[FILENAME_MAX + strlen(pcExtension)];
   char cDelimiter[] = ",;";

   printf("Choose File to open (Without file Extension!\n");
   scanf("%s", cFilename);
   strcat(cFilename, pcExtension);
   pf = fopen(cFilename, "r");

   fStrlen(cFilename);
   fStrstr(cFilename, pcExtension);
   fstrlwr(cFilename);
   fstrupr(cFilename);
   fStrtok(pf, cDelimiter);
}

/**
@fn void fStrstr(char ac[], char *pcNeedle)
@brief Searches for String in another String
@param char ac[], char *pcNeedle
@return
@author Mike
@date 16.12.2017
*/
void fStrstr(char ac[], char *pcNeedle)
{
   printf("Searches for String Pattern in another String\n");
   if (strstr(ac, pcNeedle)) {
      printf("Filename %s has File Extension %s\n", ac, pcNeedle);
   }
   else {
      printf("Pattern does not match\n");
   }
}

/**
@fn void fstrupr(char ac[])
@brief Print Output in UPPER CASE
@param char ac[]
@return
@author Mike
@date 16.12.2017
*/
void fstrupr(char ac[])
{
   printf("String to upper case: %s\n", strupr(ac));
}

/**
@fn void fstrlwr(char ac[])
@brief Print Output in lower case
@param char ac[]
@return
@author Mike
@date 16.12.2017
*/
void fstrlwr(char ac[])
{
   printf("String to lower case: %s\n", strlwr(ac));
}

/**
@fn void fStrlen(char ac[])
@brief Print string length
@param char ac[]
@return
@author Mike
@date 16.12.2017
*/
void fStrlen(char ac[])
{
   int i;
   i = strlen(ac);
   printf("STRING LENGTH: %i\n", i);
}

/**
@fn void fStrtok(FILE *pFILE, char acDelimiter[])
@brief Splits input values ​​based on a given separator
@param FILE *pFILE, char acDelimiter[]
@return
@author Mike
@date 16.12.2017
*/
void fStrtok(FILE *pFILE, char acDelimiter[])
{
   char acLine[FILENAME_MAX];
   char *pToken;
   printf("STRTOK FUNCTION:\n\n");
   if (pFILE != NULL) {
      while (feof(pFILE) == NULL) {
         fgets(acLine, 255, pFILE); // file gets
//         printf("Print acLine: %s\n\n", acLine);
         pToken = strtok(acLine, acDelimiter);

         while (pToken != NULL) {
            printf("Print pToken: %s\n", pToken);
            pToken = strtok(NULL, acDelimiter);
         }
      }
      fclose(pFILE);
   }
}

