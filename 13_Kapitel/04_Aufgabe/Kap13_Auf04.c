/**
@file: Kap13_Auf04.c
@brief: Contains main program
@author: Mike
@date: 16.12.2017
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const char sscZero = '\0';

char *fForStrncpy(char *pcDest, const char *pcSrc, size_t sN);
char *fForStrncat(char *pcDest, const char *pcSrc, size_t sN);

char *fForStrtok(char *pc, const char *pcDelimiter);
/**
@fn int main(void)
@brief main function
@param void
@return EXIT_SUCCESS (0)
@author Mike
@date 16.12.2017
*/
int main(void)
{
   char *pcTestTarget[] = {};
   char pcTestSource[] = "hello world";
   fForStrncpy(pcTestTarget, pcTestSource, 3);
   printf("pcTestTarget: %s\n", pcTestTarget);

   char pcTestSource2[] = "in C";
   char pcTestTarget2[] = "hello";
   fForStrncat(pcTestTarget2, pcTestSource2, 2);
   printf("pcTestTarget2: %s\n", pcTestTarget2);


   return EXIT_SUCCESS;
}


/**
@fn char *fStrncpy(char *pcDest, const char *pcSrc, size_t iN)
@brief Strncpy implementation
@param char *pcDest, const char *pcSrc, size_t iN
@return pcDest
@author Mike
@date 16.12.2017
*/
char *fForStrncpy(char *pcDest, const char *pcSrc, size_t sN)
{
   size_t iLength;

   for (iLength = 0; (iLength < sN) && (pcSrc[iLength] != sscZero); iLength++) {
      pcDest[iLength] = pcSrc[iLength];
   }
   pcDest[iLength] = sscZero;

   return pcDest;
}

char *fForStrncat(char *pcDest, const char *pcSrc, size_t sN)
{
   size_t destLength = strlen(pcSrc);
   size_t iLength;

   for (iLength = 0; (iLength < sN) && (pcSrc[iLength] != sscZero); iLength++) {
      pcDest[iLength + destLength] = pcSrc[iLength];
   }
   pcDest[iLength + destLength] = sscZero;
   return pcDest;
}