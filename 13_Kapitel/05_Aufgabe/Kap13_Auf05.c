
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *simple_strtok(char *, const char *);

int main(void)
{
   char *pcOut;
   char cDel[] = ",";
   char ac[] = "Hallo, in, C";

   pcOut = simple_strtok(ac, cDel);
   printf("%s\n", pcOut);

   return EXIT_SUCCESS;
}

char *simple_strtok(char *pc, const char *delim)
{
   char *pcReturnPointer, *pcHold, *pcMin;

   const char *ccpCounter;
   static char *pcTemp, *pclastElement;

   if (pc) {
      pclastElement = pc + strlen(pc);
      pcTemp = pc;
   }

   if (*pcTemp == '\0') {
      pcTemp = pclastElement;
      return pcTemp;
   }

   pcReturnPointer = pcTemp;
   pcMin = pclastElement;

   for (ccpCounter = delim; *ccpCounter != '\0'; ccpCounter++) {
      pcHold = strchr(pcTemp, *ccpCounter);
      if (pcHold && pcHold < pcMin)
         pcMin = pcHold;
   }
   if (pcMin < pclastElement) {
      *pcMin++ = '\0';
      pcTemp = pcMin;
   }
   else {
      pcTemp = pclastElement;
   }
   return pcReturnPointer;
}