/**
@file: Kap13_Auf02.c
@brief: Contains main program
@author: Mike
@date: 15.12.2017
*/

#include <stdio.h>
#include <stdlib.h>

void fAtoi(int iSize, char *pcString[]);
void fAtof(int iSize, char *pcString[]);
void fAtol(int iSize, char *pcString[]);
void fstrtod(int iSize, char *pcString[]);

/**
@fn int main(void)
@brief main function
@param
@return EXIT_SUCCESS (0)
@author Mike
@date 15.12.2017
*/
int main(int argc, char *argv[])
{

   char *pc[argc - 1];
   int iArgcSize = (sizeof(pc) / sizeof(char *));

   printf("pc: %i\n", sizeof(pc));
   printf("iArgcSize: %i\n", (iArgcSize));

   for (int i = 0; i < iArgcSize; i++) {
      pc[i] = (argv[(i + 1)]);
   }

   fAtoi(iArgcSize, pc);
   fAtol(iArgcSize, pc);
   fAtof(iArgcSize, pc);
   fstrtod(iArgcSize, pc);
   return EXIT_SUCCESS;
}

/**
@fn void fstrtod(int iSize, char *pcString[])
@brief strtod function. Split Alphanumeric String into Integer and String
@param int iSize, char *pcString[]
@return
@author Mike
@date 15.12.2017
*/
void fstrtod(int iSize, char *pcString[])
{
   double iTemp[iSize];
   char *ptr[iSize];
   for (int i = 0; i < iSize; i++) {
      iTemp[i] = strtod(pcString[i], &ptr[i]);
      printf("STRTOD Number: %lf\n", (iTemp[i]));
      printf("STRTOD Pointer: %s\n", ptr[i]);
      printf("STRTOD Pointer: %p\n", &ptr[i]);
   }
}

/**
@fn void fAtof(int iSize, char *pcString[])
@brief Atof function. Convert String to float
@param int iSize, char *pcString[]
@return
@author Mike
@date 15.12.2017
*/
void fAtof(int iSize, char *pcString[])
{
   float iTemp[iSize];
   for (int i = 0; i < iSize; i++) {
      iTemp[i] = (float) atof(pcString[i]);
      printf("ATOF: %lf\n", (iTemp[i]));
   }
}

/**
@fn void fAtol(int iSize, char *pcString[])
@brief Atol function. Convert String to Long Integer
@param int iSize, char *pcString[]
@return
@author Mike
@date 15.12.2017
*/
void fAtol(int iSize, char *pcString[])
{
   long int iTemp[iSize];
   for (int i = 0; i < iSize; i++) {
      iTemp[i] = atol(pcString[i]);
      printf("ATOL: %li\n", (iTemp[i]));
   }
}

/**
@fn void fAtoi(int iSize, char *pcString[])
@brief Atoi function. Convert String to Integer
@param int iSize, char *pcString[]
@return
@author Mike
@date 15.12.2017
*/
void fAtoi(int iSize, char *pcString[])
{
   int iTemp[iSize];
   for (int i = 0; i < iSize; i++) {
      iTemp[i] = atoi(pcString[i]);
      printf("ATOI: %i\n", (iTemp[i]));
   }
}