cmake_minimum_required(VERSION 3.8)
project(02_Aufgabe)

set(CMAKE_C_STANDARD 99)

set(SOURCE_FILES Kap13_Auf02.c)
add_executable(02_Aufgabe ${SOURCE_FILES})