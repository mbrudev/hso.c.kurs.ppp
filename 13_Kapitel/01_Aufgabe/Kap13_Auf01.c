/**
@file: Kap13_Auf01
@brief: Contains main program
@author: Mike
@date: 15.12.2017
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SIZE 10000

int *initArray();
int *buublesort(int *);
void printOutput(int *);

/**
@fn int main(void)
@brief main function
@param void
@return
@author Mike
@date 15.12.2017
*/
int main(void)
{
   double end_t;
   printf("Start Programm...\n");
   int *pdRand = initArray();
   int *pdOutput = buublesort(pdRand);
   printOutput(pdOutput);
   end_t = clock();
   printf("Time: %lf\n", (end_t / CLOCKS_PER_SEC));
   return EXIT_SUCCESS;
}

/**
@fn int *initArray(void)
@brief initialize Array
@param void
@return int aiVector
@author Mike
@date 15.12.2017
*/
int *initArray(void)
{
   static int aiVector[SIZE];
   for (int i = 0; i < SIZE; i++) {
      aiVector[i] = (rand());
   }
   return aiVector;
}

/**
@fn int *buublesort(int *pi)
@brief sort array via buublesort
@param int *pi
@return pi
@author Mike
@date 15.12.2017
*/
int *buublesort(int *pi)
{
   unsigned int uiHelper;

   for (int i = 0; i < SIZE; i++) {
      for (int j = 0; j < SIZE; j++) {
         if (pi[i] < pi[j]) {
            uiHelper = (unsigned) pi[i];
            pi[i] = pi[j];
            pi[j] = uiHelper;
         }
      }
   }
   return pi;
}

/**
@fn int printOutput(int *pi)
@brief print Output
@param int *pi
@return
@author Mike
@date 15.12.2017
*/
void printOutput(int *pi)
{
   printf("\nBubblesort:\n");
   for (int i = 0; i < SIZE; i++) {
      printf("%i\n", pi[i]);
   }
}