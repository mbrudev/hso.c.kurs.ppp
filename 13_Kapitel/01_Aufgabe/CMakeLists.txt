cmake_minimum_required(VERSION 3.8)
project(04_Aufgabe)

set(CMAKE_C_STANDARD 99)

set(SOURCE_FILES Kap13_Auf01.c)
add_executable(04_Aufgabe ${SOURCE_FILES})