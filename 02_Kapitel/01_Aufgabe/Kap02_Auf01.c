/**
 * @brief
 * @anchor Mike Bruder
 * @package 01_Aufgabe
 * @file Kap02_Auf01
 * @date 2017.10.21
 */

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

/**
 * @brief
 * @return
 */

int main(void)
{
int i;
float fPrice = 4.21F;

// Output of a string
printf("First program");

// Reading a value and output of values
scanf("%d", &i);
printf("i = %d, fPrice = %f", i, fPrice);

// Wait for user interaction
_getch();

return EXIT_SUCCESS;
}