#include <stdio.h>
#include <conio.h>

int main(void) {

    // Declare variables
    char in;
    char * str[3] = {"C", "ist die beste", "Programmiersprache"};

    //Output of a String
    printf("Welcome! Press Any Key to Continue!");

    /*
     * Iteration of a String:
     *  Reading a value
     *  Print String[i]
     *  Wait for user interaction
     */
    for (int i = 0; i <= 2; ++i) {
        scanf_s("d", &in);
        printf("%s\n", str[i]);
        _getch();
    }
}