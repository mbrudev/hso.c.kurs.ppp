#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <math.h>
#include "include_Header/utilities.h"


int main(void) {

    // Declare variables
    char in;
    char * str[3] = {"C", "ist die beste", "Programmiersprache"};

    //Output of a String
    printf("Welcome! Press Any Key to Continue!");

    /*
     * Iteration of a String:
     *  Reading a value
     *  Print String[i]
     *  Wait for user interaction
     */
    for (int i = 0; i <= 2; ++i) {
        scanf_s("d", &in);
        _clrscr();
        _gotoxy((short)powl((i+2),2), (short)powl((i+2),2));
        printf("%s\n", str[i]);
        _getch();
    }
    return EXIT_SUCCESS;
}