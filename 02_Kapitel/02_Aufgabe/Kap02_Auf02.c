/**
 * @brief
 * @anchor Mike Bruder
 * @package 02_Aufgabe
 * @file Kap02_Auf02
 * @date 2017.10.21
 */

#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <math.h>

/**
 * @brief
 * @return
 */

int main(void) {

// Declare variables
    double d;
    float fPrice = 4.21F;

/*
 * Output of a string
 */
    printf("First program\n=============\n");

// Reading a value and output of values.
    scanf_s("%lf", &d);

/*
 * Call method powl in math.h
 * Typecast Method powl to receive double number
 */
    printf("d = %lf, d*d = %lf , d*d*d = %lf, fPrice = %f", d, (double) powl(d, 2), (double) powl(d, 3), fPrice);

// Wait for user interaction
    _getch();

    return EXIT_SUCCESS;
}
