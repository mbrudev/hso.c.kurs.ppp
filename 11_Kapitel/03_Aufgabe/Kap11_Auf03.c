/**
@file: Kap11_Auf03.c
@brief:
@author: mike
@date: 12/13/17
*/
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

#define DOSHOWOUTPUT 1
/**
@fn int main(void)
@brief Preprocessor
@param void
@return EXIT_SUCCESS (0)
@author mike
@date 12/13/17
*/
int main(void)
{
   printf("Hello World!\n");
#if DOSHOWOUTPUT
   printf("Hello user!\n");
#endif
#if SHOW_GETCH
   // Wait for user interaction
   _getch();
#endif
   return EXIT_SUCCESS;
}