/**
@file: main.c
@brief: Contains main program
@author: mike
@date: 12/7/17
*/

#define SIZE 10

#include <stdio.h>
#include <stdlib.h>

/**
@fn int main(void)
@brief main function
@param void
@return EXIT_SUCCESS (0)
@author mike
@date 12/7/17
*/

int main(void)
{
   int iVal = 1;

   int aiSize[SIZE][SIZE];
   for (int i = 0; i < SIZE; i++) {
      for (int j = 0; j < SIZE; j++) {
         aiSize[i][j] = iVal;
         iVal++;
         printf("%i\n", aiSize[i][j]);
      }
   }
   return EXIT_SUCCESS;
}