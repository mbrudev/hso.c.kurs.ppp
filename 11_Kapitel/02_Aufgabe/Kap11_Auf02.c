/**
@file: Kap11_Auf02.c
@brief: Contains main program
@author: mike
@date: 12/7/17
*/

#define MAX(a, b) ((a) > (b) ? (a) : (b))

#include <stdio.h>
#include <stdlib.h>

/**
@fn int main(void)
@brief main function
@param void
@return
@author mike
@date 12/7/17
*/
int main(void)
{
   int iA = 10;
   int iB = 11;
   float fA = 2.7F;
   float fB = 3.1F;
   double dA = 666.666;
   double dB = 88.88;

   printf("iA > iB ?: %d\n", MAX(iA, iB));
   printf("iA > iB ?: %lf\n", MAX(fA, fA));
   printf("iA > iB ?: %.3lf\n", MAX(dA, dB));

}