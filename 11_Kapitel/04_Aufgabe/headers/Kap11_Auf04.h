/**
@file compileInfo.h
@brief CompileInfo
@author mike 
@date 12/7/17
*/

#include <stdio.h>

void compileInfo()
{
   printf("Filename: %s\nTime: %s\nDate: %s\n", __FILE__, __TIME__, __DATE__);
}