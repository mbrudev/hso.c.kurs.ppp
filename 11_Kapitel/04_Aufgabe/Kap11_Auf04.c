/**
@file: Kap09_Auf04.c
@brief: Contains main program
@author: mike
@date: 12/7/17
*/

#include <stdio.h>
#include <stdlib.h>
#include "headers/Kap11_Auf04.h"


/**
@fn int main(void)
@brief main fucntion
@param void
@return EXIT_SUCCESS (0)
@author mike
@date 12/7/17
*/
int main(void)
{
   compileInfo();
   return EXIT_SUCCESS;
}

